---
title: "hello"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(assertthat)
```

<!--
 You need to run the 'description' chunk in the '0-dev_history.Rmd' file before continuing your code there.
-->

# Dire Bonjour à qqun

```{r function-dire_bjr}
#' dire_bjr Renvoyer une string Bonjour suivi d'un prenom
#'
#' @param personne Prenom de la personne a saluer
#'
#' @importFrom glue glue
#' @importFrom assertthat assert_that
#' @return string
#' @export
#'
#' @examples
#' dire_bjr(personne = 'TOTO')
dire_bjr <- function(personne = 'Toi') {
  assertthat::assert_that(is.character(personne))
  stopifnot(length(personne) == 1)
  
  my_str <- glue::glue("Bonjour {personne}")
  message(my_str)
  return(my_str)
}
```

```{r examples-dire_bjr}
dire_bjr(personne = "Seb")
```

```{r tests-dire_bjr}
test_that("dire_bjr works", {
  expect_message(dire_bjr(), "Bonjour Toi")
  expect_message(dire_bjr(personne = "Toto"), "Bonjour Toto")
  expect_is(dire_bjr(), "character")
})

test_that("dire_bjr doesn't work", {
  expect_error(dire_bjr(1))
  expect_error(dire_bjr(c("a", "b")))
})
```


```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_minimal.Rmd", vignette_name = "Minimal")
```
