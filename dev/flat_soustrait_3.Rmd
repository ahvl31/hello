---
title: "flat_soustrait_3.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(assertthat)
```

```{r development-1}
# Load already included functions
pkgload::load_all(export_all = FALSE)
```

# My function

```{r function-soustrait_3}
#' soustrait_3 Title
#'
#' @param a 
#' @param b 
#' @param c 
#'
#' @return 1
#' @export
#' @importFrom assertthat assert_that
#'
#' @examples
soustrait_3 <- function(a = 1, b= 1, c= 1, d=1) {
  assert_that(is.number(a))
  assert_that(is.number(b))
  assert_that(is.number(c))
  assert_that(is.number(d))
  
  return(a-b-c-d)
}
```

```{r examples-soustrait_3}
soustrait_3()
```

```{r tests-soustrait_3}
test_that("soustrait_3 works", {
  expect_equal(soustrait_3(), -2)
})
```


```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_soustrait_3.Rmd", vignette_name = "Soustrait_3")
```

